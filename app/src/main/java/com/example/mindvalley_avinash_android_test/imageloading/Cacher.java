package com.example.mindvalley_avinash_android_test.imageloading;

import android.support.v4.util.LruCache;

public class Cacher<T> {
    private LruCache<String, T> cache;

    public Cacher() {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        // we should't be greedy to exhaust completely
        // so we shall use as per necessity
        int cacheSize = maxMemory / 8;
        cache = new LruCache<String, T>(cacheSize);
    }

    public T findInCache(String url) {
        return cache.get(url);
    }

    public void saveToCache(String url, T t) {
        if (findInCache(url) == null) {
            cache.put(url, t);
        }
    }
}
