package com.example.mindvalley_avinash_android_test.imageloading;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;

public class ImageLoaderTask extends AsyncTask<Void, Void, Bitmap> {

    private final ImageView thumbnail;
    private final String url;
    private Cacher<Bitmap> imageCache;

    public ImageLoaderTask(ImageView thumbnail, String url) {
        this.thumbnail = thumbnail;
        this.url = url;
        imageCache = new Cacher();
    }


    @Override
    protected Bitmap doInBackground(Void... params) {
        String imageURL = url;
        Bitmap bitmap = imageCache.findInCache(imageURL);
        if (bitmap != null) {
            // already present in cache
        } else {
            try {
                InputStream input = new java.net.URL(imageURL).openStream();
                bitmap = BitmapFactory.decodeStream(input);
                // after downloading & scaling save the image to cache
                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, false);
                imageCache.saveToCache(imageURL, bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // either in cache or downloaded
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bmp) {
        super.onPostExecute(bmp);
        thumbnail.setImageBitmap(bmp);
    }

}
