package com.example.mindvalley_avinash_android_test.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mindvalley_avinash_android_test.R;
import com.example.mindvalley_avinash_android_test.Util.NetworkUtil;
import com.example.mindvalley_avinash_android_test.Util.SnackBar;
import com.example.mindvalley_avinash_android_test.activities.GalleryPagerActivity;
import com.example.mindvalley_avinash_android_test.imageloading.ImageLoaderTask;
import com.example.mindvalley_avinash_android_test.model.Post;

import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {

    public static final String URL = "url";
    private final Context context;
    private List<Post> posts;

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;

        public ImageViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }

    }


    public ImagesAdapter(List<Post> posts, Context context) {
        this.posts = posts;
        this.context = context;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_item_image_card, parent, false);
        return new ImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, int position) {
        final Post post = posts.get(position);
        holder.title.setText(post.getUser().getName());
        ImageLoaderTask task;
        task = (ImageLoaderTask) new ImageLoaderTask(holder.thumbnail, post.getUser().getProfileImage().getMedium()).execute();
        // we can call
        // task.cancel(true);
        // at anytime can stop loading the image
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GalleryPagerActivity.class);
                String[] urls = new String[]{post.getUser().getProfileImage().getLarge(), post.getUrls().getRegular()};
                intent.putExtra(URL, urls);
                if (NetworkUtil.isOnline(context)) {
                    ActivityCompat.startActivity((Activity) context, intent, getOptions(v).toBundle());
                } else {
                    SnackBar.showSnackBar(v, R.string.turn_on_internet).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    private ActivityOptionsCompat getOptions(View view) {
        return ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
                view, context.getString(R.string.transition_string));
    }
}
