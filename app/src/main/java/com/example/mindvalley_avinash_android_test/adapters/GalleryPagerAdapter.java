package com.example.mindvalley_avinash_android_test.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mindvalley_avinash_android_test.fragments.GalleryFragment;

public class GalleryPagerAdapter extends FragmentPagerAdapter {

    private String[] images;

    public GalleryPagerAdapter(FragmentManager fm, String[] images) {
        super(fm);
        this.images = images;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(ImagesAdapter.URL, images[position]);
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public int getCount() {
        return images.length;
    }
}
