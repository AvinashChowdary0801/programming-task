package com.example.mindvalley_avinash_android_test.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.example.mindvalley_avinash_android_test.R;
import com.example.mindvalley_avinash_android_test.activities.ImagesActivity;

public class SplashScreenActivity extends AppCompatActivity {

    private static final long THREE_THOUSAND = 3000;

    private DemoSurfaceView demoSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        demoSurfaceView = new DemoSurfaceView(this);
        container.addView(demoSurfaceView);
        Runnable splashRunnable = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, ImagesActivity.class));
                SplashScreenActivity.this.finish();
            }
        };
        new Handler().postDelayed(splashRunnable, THREE_THOUSAND);
    }
}
