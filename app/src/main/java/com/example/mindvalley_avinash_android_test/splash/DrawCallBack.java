package com.example.mindvalley_avinash_android_test.splash;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface DrawCallBack {
    void doDraw(Canvas canvas, Paint paint);

    void reshape(int width, int height);
}