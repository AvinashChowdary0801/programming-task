package com.example.mindvalley_avinash_android_test.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.mindvalley_avinash_android_test.R;
import com.example.mindvalley_avinash_android_test.Util.NetworkStateChangeListener;
import com.example.mindvalley_avinash_android_test.Util.NetworkStateListener;
import com.example.mindvalley_avinash_android_test.Util.NetworkUtil;
import com.example.mindvalley_avinash_android_test.Util.SnackBar;
import com.example.mindvalley_avinash_android_test.adapters.ImagesAdapter;
import com.example.mindvalley_avinash_android_test.imageloading.Cacher;
import com.example.mindvalley_avinash_android_test.model.Post;
import com.example.mindvalley_avinash_android_test.rest.RestClient;
import com.example.mindvalley_avinash_android_test.rest.Services;
import com.google.gson.Gson;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ImagesActivity extends AppCompatActivity implements NetworkStateChangeListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int NETWORK = 121;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private boolean isDataFetched;
    private Cacher<String> postCache;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.txt_images));
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);

        if (isNetworkGranted(this) && NetworkUtil.isOnline(this)) {
        getPosts();
        } else {
            SnackBar.showSnackBar(toolbar, R.string.turn_on_internet).show();
        }

    }

    @Override
    public void onRefresh() {
        getPosts();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isNetworkGranted(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.INTERNET}, NETWORK);
            return false;
        } else {
            return true;
        }
    }

    private void getPosts() {
        swipeRefreshLayout.setRefreshing(true);
        postCache = new Cacher<>();
        if (postCache.findInCache(RestClient.BASE_URL) != null) {
            setAdapter((List<Post>) new Gson().fromJson(postCache.findInCache(RestClient.BASE_URL), Post.class));
        } else {
            Services Services = RestClient.getInstance().getSVService();
            Services.getPosts(new Callback<List<Post>>() {
                @Override
                public void success(List<Post> posts, Response response) {
                    swipeRefreshLayout.setRefreshing(false);
                    String data = new Gson().toJson(posts);
                    postCache.saveToCache(RestClient.BASE_URL, data);
                    setAdapter(posts);
                }

                @Override
                public void failure(RetrofitError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (error.getResponse() != null && error.getResponse().getBody() != null) {
                        SnackBar.showSnackBar(toolbar, R.string.error_retreiving_data).show();
                    }
                }
            });
        }
    }

    private void setAdapter(List<Post> posts) {
        if (posts != null && posts.size() > 0) {
            isDataFetched = true;
            recyclerView.setAdapter(new ImagesAdapter(posts, this));
        } else {
            SnackBar.showSnackBar(toolbar, R.string.error_retreiving_data).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NetworkStateListener.registerNetworkState(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NetworkStateListener.unregisterNetworkState(this);
    }

    @Override
    public void onNetworkStateChanged(int networkState) {
        if (networkState == NetworkStateListener.NETWORK_CONNECTED) {
            if (!isDataFetched) {
                getPosts();
            }
        }
    }
}
