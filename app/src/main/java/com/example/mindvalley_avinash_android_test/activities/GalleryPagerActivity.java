package com.example.mindvalley_avinash_android_test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.mindvalley_avinash_android_test.R;
import com.example.mindvalley_avinash_android_test.adapters.GalleryPagerAdapter;
import com.example.mindvalley_avinash_android_test.adapters.ImagesAdapter;
import com.pixelcan.inkpageindicator.InkPageIndicator;

public class GalleryPagerActivity extends AppCompatActivity {

    private ViewPager galleryPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_pager);

        Intent intent = getIntent();
        if (getIntent() != null) {
            String[] gallery = intent.getStringArrayExtra(ImagesAdapter.URL);
            if (gallery.length > 0) {
                initPager(gallery, 0);
            }
        }

    }

    /**
     * Initialize pager and set indicator
     * call adapter which gets new fragment for each item of viewpager
     *
     * @param images
     */
    private void initPager(String[] images, int pos) {

        galleryPager = (ViewPager) findViewById(R.id.pager_gallery);
        GalleryPagerAdapter adapter = new GalleryPagerAdapter(getSupportFragmentManager(), images);
        galleryPager.setAdapter(adapter);
        InkPageIndicator inkPageIndicator = (InkPageIndicator) findViewById(R.id.indicator);
        inkPageIndicator.setViewPager(galleryPager);
        galleryPager.setCurrentItem(pos, true);
    }

}
