package com.example.mindvalley_avinash_android_test.rest;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class RestClient implements RequestInterceptor {

    public static final String BASE_URL = "http://pastebin.com";

    public static RestClient getInstance() {
        return new RestClient();
    }

    public Services getSVService() {
        // create a rest adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                // visibility of log
                // in order to verify response
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(this)
                .setClient(new retrofit.client.UrlConnectionClient())
                .build();
        // add it to services
        // api calls
        return restAdapter.create(Services.class);
    }

    @Override
    public void intercept(RequestFacade request) {
        //
    }
}
