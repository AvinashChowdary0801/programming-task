package com.example.mindvalley_avinash_android_test.rest;

import com.example.mindvalley_avinash_android_test.model.Post;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

public interface Services {

    @GET("/raw/wgkJgazE")
    void getPosts(Callback<List<Post>> callBack);

}
