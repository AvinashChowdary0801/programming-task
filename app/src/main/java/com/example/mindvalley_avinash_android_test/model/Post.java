package com.example.mindvalley_avinash_android_test.model;

import com.google.gson.annotations.SerializedName;

public class Post {

    private String id;

    private String height;

    @SerializedName("current_user_collections")
    private String[] currentUserCollections;

    private String color;

    private Urls urls;

    private String likes;

    private String width;

    private String created_at;

    private Links links;

    private Categories[] categories;

    private User user;

    @SerializedName("liked_by_user")
    private String likedByUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String[] getCurrentUserCollections() {
        return currentUserCollections;
    }

    public void setCurrentUserCollections(String[] currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Categories[] getCategories() {
        return categories;
    }

    public void setCategories(Categories[] categories) {
        this.categories = categories;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(String likedByUser) {
        this.likedByUser = likedByUser;
    }
}
