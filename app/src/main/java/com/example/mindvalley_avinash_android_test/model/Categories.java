package com.example.mindvalley_avinash_android_test.model;

import com.google.gson.annotations.SerializedName;

public class Categories {

    private String id;

    private String title;

    private Links links;

    @SerializedName("photo_count")
    private String photoCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public String getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(String photoCount) {
        this.photoCount = photoCount;
    }
}
