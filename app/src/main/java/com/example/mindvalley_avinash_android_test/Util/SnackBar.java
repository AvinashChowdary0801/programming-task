package com.example.mindvalley_avinash_android_test.Util;

import android.support.design.widget.Snackbar;
import android.view.View;

public class SnackBar {

    private static Snackbar snackBar;

    private SnackBar() {
        // Private constructor to avoid public
    }

    public static Snackbar showSnackBar(View view, int text) {
        snackBar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        return snackBar;
    }

    public static Snackbar showSnackBar(View view, String text) {
        snackBar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        return snackBar;
    }

}