package com.example.mindvalley_avinash_android_test.Util;

public interface NetworkStateChangeListener {
    void onNetworkStateChanged(int networkState);
}
