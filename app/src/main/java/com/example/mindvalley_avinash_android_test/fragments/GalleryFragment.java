package com.example.mindvalley_avinash_android_test.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.mindvalley_avinash_android_test.R;
import com.example.mindvalley_avinash_android_test.adapters.ImagesAdapter;
import com.example.mindvalley_avinash_android_test.imageloading.ImageLoaderTask;

public class GalleryFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String url = getArguments().getString(ImagesAdapter.URL);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        if(!TextUtils.isEmpty(url)) {
            ImageView image = (ImageView) view.findViewById(R.id.image);
            new ImageLoaderTask(image, url).execute();
        }
        return view;
    }
}
